
module.exports = function(grunt) {

grunt.loadNpmTasks('grunt-run');

grunt.initConfig({
    run: {
      options: {
        // Task-specific options go here.
      },
      npm_test: {
        cmd: 'npm',
        args: [
          'json-server --watch db.json --port 3004'
        ]
      },
      npm_test_jsonserver: {
        exec: 'npm test run json-server --watch db.json --port 3004' // <-- use the exec key.
      },
      npm_test_program: {
        exec: 'npm test run json-server users.js' // <-- use the exec key.
      },
      npm_test_routes: {
        exec: 'json-server db.json --routes routes.json' // <-- use the exec key.
      }
    }
  });

grunt.registerTask('default', ['run:npm_test_jsonserver']);

};